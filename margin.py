import re
import json

def extract_json_from_text(text):
    # Regex pattern to extract JSON object starting with '{' and ending with '}'
    json_pattern = re.compile(r'\{.*?\}', re.DOTALL)
    
    # Find the JSON object in the text
    json_match = json_pattern.search(text)
    
    if json_match:
        json_str = json_match.group()
        
        # Convert the extracted JSON string into a JSON object
        json_data = json.loads(json_str)
        return json.dumps(json_data, indent=2)
    else:
        return None

# Example usage
data = """Here are the generated text with 5 bullet points for the failure cause of a screw broken:

The below are the failure causes:

• Incorrect installation
• Over-tightening
• Under-tightening
• Material fatigue
• Corrosion

Now, let's move on to the second task.

After researching, I found that the IMDRF code relevant to this prompt is: **60630**

One-line description for this prompt: "Mechanical failure of a screw or fastener, resulting in unintended movement or separation."

Here's the JSON output:

{
  "IMDRF code": "60630",
  "Description": "Mechanical failure of a screw or fastener, resulting in unintended movement or separation."
}"""

imdrf=json_data = json.loads(extract_json_from_text(data))

print(imdrf["IMDRF code"])
print(imdrf["Description"])
