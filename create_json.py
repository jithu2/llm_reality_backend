import re
def extract_json_from_text(text):
    # Regex pattern to extract JSON object starting with '{' and ending with '}'
    json_pattern = re.compile(r'\{.*?\}', re.DOTALL)
    
    # Find the JSON object in the text
    json_match = json_pattern.search(text)
    
    if json_match:
        json_str = json_match.group()
        
        # Convert the extracted JSON string into a JSON object
        json_data = json.loads(json_str)
        return json.dumps(json_data, indent=2)
    else:
        return None

def json_create(data):
    sections = data.strip().split("\n")
    flag=False
    custom={}
    key_value=""

    cnt=0
    for each in sections:
        #print(each)
        #print("new=",each.lower(),flag)
        #print(custom)
     
        # #print("current flag",flag)
        if flag==True:
            words = each.split()
            cleaned_text = ' '.join(words[1:])  # Join all words except the first one
            text_only = re.sub(r'[^a-zA-Z\s]', '', cleaned_text)
            if len(text_only)>0:
        
                custom[key_value].append(text_only)
            
            # #print(text_only,cnt)
            if cnt==5:
                flag=False
                #print("change",flag)
            cnt+=1
        
        if "failure cause:" in each.lower():
            flag=True
            key_value="failure_cause"
            custom["failure_cause"]=[]
            cnt=0

        if "failure mode:" in each.lower():
            flag=True
            key_value="failure_mode"
            custom["failure_mode"]=[]
            cnt=0

        if "hazard:" in each.lower():
            flag=True
            key_value="hazard"
       
            custom["hazard"]=[]
            cnt=0
        if "hazard situation:" in each.lower():
            flag=True
            key_value="hazard_situation"
            custom["hazard_situation"]=[]
            cnt=0
        if "remediation:" in each.lower():
            flag=True
            key_value="remediation"
            custom["remediation"]=[]
            cnt=0
    return custom


def json_create_failure_cause(data):
    sections = data.strip().split("\n")
    flag=False
    custom={}
    key_value=""

    cnt=0
    for each in sections:
        #print(each)
        #print("new=",each.lower(),flag)
        #print(custom)
     
        # #print("current flag",flag)
        if flag==True:
            words = each.split()
            cleaned_text = ' '.join(words[1:])  # Join all words except the first one
            text_only = re.sub(r'[^a-zA-Z\s]', '', cleaned_text)
            if len(text_only)>0:
        
                custom[key_value].append(text_only)
            
            # #print(text_only,cnt)
            if cnt==5:

                
                flag=False
                #print("change",flag)
            cnt+=1
        
        if "failure causes:" in each.lower():
            flag=True
            key_value="failure_cause"
            custom["failure_cause"]=[]
            cnt=0
    return custom

def json_create_failure_mode(data):
    sections = data.strip().split("\n")
    flag=False
    custom={}
    key_value=""

    cnt=0
    for each in sections:
        #print(each)
        #print("new=",each.lower(),flag)
        #print(custom)
     
        # #print("current flag",flag)
        if flag==True:
            words = each.split()
            cleaned_text = ' '.join(words[1:])  # Join all words except the first one
            text_only = re.sub(r'[^a-zA-Z\s]', '', cleaned_text)
            if len(text_only)>0:
        
                custom[key_value].append(text_only)
            
            # #print(text_only,cnt)
            if cnt==5:

                
                flag=False
                #print("change",flag)
            cnt+=1
        
        if "failure modes:" in each.lower():
            flag=True
            key_value="failure_mode"
            custom["failure_mode"]=[]
            cnt=0
    return custom


def json_create_hazard(data):
    sections = data.strip().split("\n")
    flag=False
    custom={}
    key_value=""

    cnt=0
    for each in sections:
        #print(each)
        #print("new=",each.lower(),flag)
        #print(custom)
     
        # #print("current flag",flag)
        if flag==True:
            words = each.split()
            cleaned_text = ' '.join(words[1:])  # Join all words except the first one
            text_only = re.sub(r'[^a-zA-Z\s]', '', cleaned_text)
            if len(text_only)>0:
        
                custom[key_value].append(text_only)
            
            # #print(text_only,cnt)
            if cnt==5:

                
                flag=False
                #print("change",flag)
            cnt+=1
        
        if "hazards:" in each.lower():
            flag=True
            key_value="hazard"
            custom["hazard"]=[]
            cnt=0
    return custom


def json_create_hazard_situation(data):
    sections = data.strip().split("\n")
    flag=False
    custom={}
    key_value=""

    cnt=0
    for each in sections:
        #print(each)
        #print("new=",each.lower(),flag)
        #print(custom)
     
        # #print("current flag",flag)
        if flag==True:
            words = each.split()
            cleaned_text = ' '.join(words[1:])  # Join all words except the first one
            text_only = re.sub(r'[^a-zA-Z\s]', '', cleaned_text)
            if len(text_only)>0:
        
                custom[key_value].append(text_only)
            
            # #print(text_only,cnt)
            if cnt==5:

                
                flag=False
                #print("change",flag)
            cnt+=1
        if "hazard situations:" in each.lower():
            flag=True
            key_value="hazard_situation"
            custom["hazard_situation"]=[]
            cnt=0
    return custom

def json_create_remediation(data):
    sections = data.strip().split("\n")
    flag=False
    custom={}
    key_value=""

    cnt=0
    for each in sections:
        #print(each)
        #print("new=",each.lower(),flag)
        #print(custom)
     
        # #print("current flag",flag)
        if flag==True:
            words = each.split()
            cleaned_text = ' '.join(words[1:])  # Join all words except the first one
            text_only = re.sub(r'[^a-zA-Z\s]', '', cleaned_text)
            if len(text_only)>0:
        
                custom[key_value].append(text_only)
            
            # #print(text_only,cnt)
            if cnt==5:

                
                flag=False
                #print("change",flag)
            cnt+=1
        
        if "remediations:" in each.lower():
            flag=True
            key_value="remediations"
            custom["remediations"]=[]
            cnt=0
    
    return custom