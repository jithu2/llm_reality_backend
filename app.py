from fastapi import FastAPI, HTTPException, Header, status
from fastapi.middleware.cors import CORSMiddleware
from create_json import (
    json_create,
    json_create_failure_cause,
    json_create_failure_mode,
    json_create_hazard,
    json_create_hazard_situation,
    json_create_remediation
)
import uvicorn
from pydantic import BaseModel
import time
import requests
import json
import os

# Load token from environment variables
api_key_value = os.getenv("API_KEY_VALUE", "22AA43")
valid_client_id = "DATAR"

# URL of the API endpoint
url = 'http://localhost:11434/api/generate'

app = FastAPI()

# CORS Middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],  # Replace with your React app URL
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Define Pydantic model for form data
class InputData(BaseModel):
    input_data: str

def generate_text_response(output_prompt: str, required_words: list):
    response_content = ""
    data_input = {
        "model": "llama3",
        "prompt": output_prompt
    }

    while True:
        response_content = ""
        response = requests.post(url, json=data_input, stream=True)

        if response.status_code == 200:
            for line in response.iter_lines():
                if line:
                    line_json = json.loads(line.decode('utf-8'))
                    response_content += line_json.get("response", "")

        data = response_content.strip()

        if all(word in data.lower() for word in required_words):
            break
    
    return data

@app.post("/generate_text")
async def generate_text(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    output_prompt = (
        f"""my prompt is '{input_data.input_data}'
        what are the 1. Failure Cause:, 2. Failure mode:, 3. Hazard:, 4. Hazard situation:, 5. Remediation:
        generated text in these categories
        with 5 bullet points in each category: no need for explanation just bullet points."""
    )

    required_words = ["failure cause:", "failure mode:", "hazard:", "hazard situation:", "remediation:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create(data=data)
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/failure_cause")
async def failure_cause(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    output_prompt = (
        f"""my prompt is '{input_data.input_data}'. What are the Failure causes: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the Failure causes:"""
    )

    required_words = ["failure causes:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_failure_cause(data=data)
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/failure_mode")
async def failure_mode(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    output_prompt = (
        f"""my prompt is '{input_data.input_data}'. What are the Failure modes: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the Failure modes:"""
    )

    required_words = ["failure modes:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_failure_mode(data=data)
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/hazard")
async def hazard(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    output_prompt = (
        f"""my prompt is '{input_data.input_data}'. What are the Hazards: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the Hazards:"""
    )

    required_words = ["hazards:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_hazard(data=data)
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/hazard_situation")
async def hazard_situation( input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    output_prompt = (
        f"""my prompt is '{input_data.input_data}'. What are the Hazard situations: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the Hazard situations:"""
    )

    required_words = ["hazard situations:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_hazard_situation(data=data)
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/remediation")
async def remediations(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    output_prompt = (
        f"""my prompt is '{input_data.input_data}'. What are the Remediations: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the Remediations:"""
    )

    required_words = ["remediations:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_remediation(data=data)
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
