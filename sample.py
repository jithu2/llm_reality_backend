from fastapi import FastAPI, HTTPException, Header, status
from fastapi.middleware.cors import CORSMiddleware
from test import (
    json_create,
    json_create_failure_cause,
    json_create_failure_mode,
    json_create_hazard,
    json_create_hazard_situation,
    json_create_remediation,
    extract_json_from_text
)
import uvicorn
import json

from pydantic import BaseModel
import time
import requests
import json
import os

# Load token from environment variables
api_key_value = os.getenv("API_KEY_VALUE", "22AA43")
valid_client_id = "DATAR"

# URL of the API endpoint
url = "http://localhost:11434/api/generate"

app = FastAPI()

# CORS Middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],  # Replace with your React app URL
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Define Pydantic model for form data
class InputData(BaseModel):
    input_data: str

def generate_text_response(output_prompt: str, required_words: list):
    response_content = ""
    data_input = {
        "model": "llama3",
        "prompt": output_prompt
    }

    while True:
        response_content = ""
        response = requests.post(url, json=data_input, stream=True)

        if response.status_code == 200:
            for line in response.iter_lines():
                if line:
                    line_json = json.loads(line.decode("utf-8"))
                    response_content += line_json.get("response", "")

        data = response_content.strip()
        print(data)
        print("___________________________________________________________________________________________")

        if all(word in data.lower() for word in required_words):
            break
      

    
    return data



@app.post("/failure_cause")
async def failure_cause(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()
    int_promt=f"""my prompt is "{input_data.input_data}". What are the failure cause: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the failure causes:
        sencond task is find the imdrf(International Medical Device Regulators Forum) code and one line description for this promt
        fill the json:
     """
    new=int_promt+"""{   "IMDRF code" : "",
                          "Description": ""}"""

    output_prompt = (
        new
    )

    required_words = ["failure causes:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_failure_cause(data=data)
    get_imdrf=extract_json_from_text(data)
    print(get_imdrf)
    json_data_imdrf = json.loads(get_imdrf)
    json_data["IMDRF code"]=json_data_imdrf["IMDRF code"]
    json_data["Description"]=json_data_imdrf["Description"]
   


    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/failure_mode")
async def failure_mode(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    int_promt=f"""my prompt is "{input_data.input_data}". What are the failure mode: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the failure modes:
        sencond task is find the imdrf(International Medical Device Regulators Forum) code and one line description for this promt
        fill the json:
     """
    new=int_promt+"""{   "IMDRF code" : "",
                          "Description" : ""}"""

    output_prompt = (
        new
    )

    required_words = ["failure modes"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_failure_mode(data=data)
    get_imdrf=extract_json_from_text(data)
    json_data_imdrf = json.loads(get_imdrf)
    json_data["IMDRF code"]=json_data_imdrf["IMDRF code"]
    json_data["Description"]=json_data_imdrf["Description"]
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/hazard")
async def hazard(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    int_promt=f"""my prompt is "{input_data.input_data}". What are the hazard: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the hazards:
        sencond task is find the imdrf(International Medical Device Regulators Forum) code and one line description for this promt
        fill the json:
     """
    new=int_promt+"""{   "IMDRF code" : "",
                          "Description": ""}"""

    output_prompt = (
        new
    )

    required_words = ["hazards:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_hazard(data=data)

    get_imdrf=extract_json_from_text(data)
    json_data_imdrf = json.loads(get_imdrf)
    json_data["IMDRF code"]=json_data_imdrf["IMDRF code"]
    json_data["Description"]=json_data_imdrf["Description"]
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/hazard_situation")
async def hazard_situation( input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    int_promt=f"""my prompt is "{input_data.input_data}". What are the hazard situation: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the hazard situations:
        sencond task is find the imdrf(International Medical Device Regulators Forum) code and one line description for this promt
        fill the json:
     """
    new=int_promt+"""{   "IMDRF code" : "",
                          "Description": ""}"""

    output_prompt = (
        new
    )


    required_words = ["hazard situations:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_hazard_situation(data=data)
    get_imdrf=extract_json_from_text(data)
    json_data_imdrf = json.loads(get_imdrf)
    json_data["IMDRF code"]=json_data_imdrf["IMDRF code"]
    json_data["Description"]=json_data_imdrf["Description"]
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

@app.post("/remediation")
async def remediations(input_data: InputData,CLIENT_ID: str = Header(None),  API_KEY: str = Header(None)):
    if API_KEY != api_key_value:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API_KEY")
    if CLIENT_ID != valid_client_id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid CLIENT_ID")

    start_time = time.time()

    int_promt=f"""my prompt is "{input_data.input_data}". What are the remediation: generated text with 5 bullet points in this category: no need for explanation, just bullet points. --start the sentence with: The below are the remediations:
        sencond task is find the imdrf(International Medical Device Regulators Forum) code and one line description for this promt
        fill the json:
     """
    new=int_promt+"""{   "IMDRF code" : "",
                          "Description": ""}"""

    output_prompt = (
        new
    )


    required_words = ["remediations:"]
    data = generate_text_response(output_prompt, required_words)

    json_data = json_create_remediation(data=data)
    get_imdrf=extract_json_from_text(data)
    json_data_imdrf = json.loads(get_imdrf)
    json_data["IMDRF code"]=json_data_imdrf["IMDRF code"]
    json_data["Description"]=json_data_imdrf["Description"]
    end_time = time.time()
    print("Time taken:", end_time - start_time)

    return {"response": json_data}

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
