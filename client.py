import aiohttp
import asyncio
import time

async def send_request(session):
    url = "http://localhost:11434/api/generate"
    input_value = "my pace maker value is 200"
    data = (
        "my prompt is 'my pace maker value is 200'. What are the Failure cause:\n"
        "generated text with 5 bullet points in this category: no need for explanation, just bulletins."
    )
    payload = {
        "model": "llama3",
        "prompt": data
    }
    start_time = time.time()
    async with session.post(url, json=payload) as response:
        response_text = await response.text()
        end_time = time.time()
        duration = end_time - start_time
        return duration, response_text

async def main():
    start_time = time.time()
    async with aiohttp.ClientSession() as session:
        tasks = [send_request(session) for _ in range(num_requests)]
        results = await asyncio.gather(*tasks)
        
        for duration, response_text in results:
            print(f"Request completed in {duration:.2f} seconds")
    
    total_duration = time.time() - start_time
    print(f"Total execution time for all requests: {total_duration:.2f} seconds")

if __name__ == "__main__":
    num_requests = 100
    asyncio.run(main())
