from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from create_json import json_create
import uvicorn
from pydantic import BaseModel
import time
import requests
import json

# URL of the API endpoint
url = 'http://localhost:11434/api/generate'

app = FastAPI()


# CORS Middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://52.35.96.12:3000"],  # Add your React app URL here
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
  
)

# Define Pydantic model for form data
class InputData(BaseModel):
    input_data: str


@app.post("/generate_text")
async def generate_text(input_data: InputData):
    start_time=time.time()
    # Combine input prompt with template
    output_prompt = f"""my promt is '{input_data.input_data}'
          what are the 1. Failure Cause:,2. Failure mode:,3. Hazard:,4. Hazard situation:,5. Remediation:

        generated text in these category
with 5 bulletin point in each category: no need to explanation just bulletin """

    # Generate text
  
    response_content = ""
    data=""
    data_input = {
    "model": "llama3",
    "prompt": output_prompt
    }

    while True:
        response_content = ""
        # Sending the POST request
        response = requests.post(url, json=data_input, stream=True)

        # Checking if the request was successful
        if response.status_code == 200:
           
            for line in response.iter_lines():
                if line:
                    # Decode the line and parse it as JSON
                    line_json = json.loads(line.decode('utf-8'))
                   
                    # Extract the "response" key and append it to the response_text
                    response_content += line_json.get("response", "")
                    
        
        
        sections = response_content.strip().split("\n")
        data = response_content
     

        required_words = ["failure cause:", "failure mode:", "hazard:", "hazard situation:", "remediation:"]
        temp=True
        for each_word in required_words:
            if each_word not in data.lower():
                temp=False
        
        if temp:
            break

        
        
        
        

    # Create JSON data
    json_data = json_create(data=data)
    end_time=time.time()
    time_diff=end_time-start_time
    print("time difference:",time_diff)

    # Return the JSON response
    return {"response": json_data}


@app.post("/failure_cause")
async def generate_text(input_data: InputData):
    start_time=time.time()
    # Combine input prompt with template
    output_prompt = f"""my prompt is '{input_data.input_data}'. What are the Failure cause: generated text with 5 bulletin points in this category: no need for explanation, just bulletins."""

    # Generate text
  
    response_content = ""
    data=""
    data_input = {
    "model": "llama3",
    "prompt": output_prompt
    }

    while True:
        response_content = ""
        # Sending the POST request
        response = requests.post(url, json=data_input, stream=True)

        # Checking if the request was successful
        if response.status_code == 200:
           
            for line in response.iter_lines():
                if line:
                    # Decode the line and parse it as JSON
                    line_json = json.loads(line.decode('utf-8'))
                   
                    # Extract the "response" key and append it to the response_text
                    response_content += line_json.get("response", "")
                    
        
        
        sections = response_content.strip().split("\n")
        data = response_content
     

        required_words = ["failure cause:"]
        temp=True
        for each_word in required_words:
            if each_word not in data.lower():
                temp=False
        
        if temp:
            break

        
        
        
        

    # Create JSON data
    json_data = json_create(data=data)
    end_time=time.time()
    time_diff=end_time-start_time
    print("time difference:",time_diff)

    # Return the JSON response
    return {"response": json_data}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
